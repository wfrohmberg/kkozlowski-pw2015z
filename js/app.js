'use strict';
(function(){
	/************************************************/
	/**[ Template ]**********************************/
	/************************************************/
	function Template(json){
		this.item = {};
		this.rootElement = this.parseJSON(json);
	};
	Template.prototype.parseJSON = function(json){
		var i, j, x, element, child;
		if(json.constructor===Array){
			if(json[0].constructor!==String){
				throw new Error(/*TODO pierwszy element tablicy musi być String'iem*/);
			}
			element = window.document.createElement(json[0]);
			for(i=1;i<json.length;i++){
				if(json[i]){
					if(json[i].constructor===Object){
						for(x in json[i]){
							element.setAttribute(x, json[i][x]);
						}
						if(element.attributes.getNamedItem('item')){
							this.item[element.attributes.getNamedItem('item').value] = element;
						}
					}else if(json[i].constructor===Array){
						for(j=0;j<json[i].length;j++){
							if(json[i][j].constructor===Array){
								element.appendChild(this.parseJSON(json[i][j]));
							}else if(json[i][j].constructor===String){
								element.appendChild(window.document.createTextNode(json[i][j]));
							}else{
								throw new Error(/*TODO w tablicy z potomkami jest coś innego jak String/Array*/);
							}
						}
					}else{
						throw new Error(/*TODO w tablicy jest coś innego jak Objekt/Array*/);
					}
				}else{
					throw new Error(/*TODO w tablicy jest udefined/null - nie ma constructor'a*/);
				}
			}
			return element;
		}else if(json.constructor===String){
			return window.document.createTextNode(text);
		}else{
			throw new Error('Template.parseJSON(): argument 0 is not instance of String/Array.');
		}
	};
	Template.prototype.appendTo = function(htmlElement){
		if(htmlElement instanceof HTMLElement){
			htmlElement.appendChild(this.rootElement);
		}else{
			throw new Error('Template.appendTo(): argument 0 is not instance of HTMLElement.');
		}
	};
	Template.prototype.appendChildTo = function(itemName, json){
		this.item[itemName].appendChild(this.parseJSON(json));
	};
	Template.prototype.remove = function(){
		this.rootElement.remove();
	};
	/************************************************/
	/**[ Layer ]*************************************/
	/************************************************/
	function Layer(config){
		this.config = config || {};
		this.view = new LayerView(this);
		this.controller = new LayerController(this);
	};
	function LayerView(parent){
		this.parent = parent;
		this.template = new Template(
			['div',{'class':'window'},[
				['div', {'class':'hWrapper vWrapper'}, [
					['div',{'item':'container','class':'vContent'}]
				]]
			]]
		);
		this.template.appendTo(window.document.body);
	};
	function LayerController(parent){
		this.parent = parent;
	};
	LayerController.prototype.close = function(){
		this.parent.view.template.remove();
	};
	/************************************************/
	/**[ Modal ]*************************************/
	/************************************************/
	function Modal(config){
		this.config = config || {};
		this.controller = new ModalController(this);
		this.view = new ModalView(this);
	};
	function ModalView(parent){
		this.template = parent.controller.layer.view.template;
		this.template.appendChildTo('container',
			['div',{'class':'modal-content','style':'margin:0 auto;max-width:'+(parent.config.modalWidth || 480)+'px;'},[
				['div',{'item':'modalHeader','class':'modal-header'},[
					['button',{'item':'buttonClose','type':'button','class':'close'},['×']],
					['h4',{'item':'header'}]
				]],
				['div',{'item':'modalBody','class':'modal-body'}],
				['div',{'item':'modalFooter','class':'modal-footer'}]
			]]
		);
		this.template.item.buttonClose.addEventListener('click', function(){
			parent.controller.close();
		}, false);
	};
	function ModalController(parent){
		this.parent = parent;
		this.layer = new Layer(parent.config);
	};
	ModalController.prototype.close = function(){
		this.parent.config.onClose && this.parent.config.onClose.apply(this, arguments);
		this.layer.controller.close();
	};
	/************************************************/
	/**[ Knapsack ]***********************************/
	/************************************************/
	function Knapsack(config){
		this.config = config || {};
		this.model = new KnapsackModel(this);
		this.controller = new KnapsackController(this);
		this.view = new KnapsackView(this);
	};
	// Model
	function KnapsackModel(parent){
		this.parent = parent;
	};
	// View
	function KnapsackView(parent){
		ModalView.apply(this, arguments);
		this.template.appendChildTo('modalHeader',['strong',['Knapsack']]);
		this.template.appendChildTo('modalBody',['div']);
		this.template.appendChildTo('modalFooter',['div',{'class':'btn-group'},[
			['button',{'class':'btn btn-success','item':'buttonStart'},[
				'Start'
			]]
		]]);
		
		this.template.item.speak.addEventListener('click',function(){
			parent.controller.speak();
		},false);
		this.template.item.inputAnswer.addEventListener('keypress',function(e){
			parent.controller.keypress(e);
		},false);
		this.template.item.buttonStart.addEventListener('click',function(){
			parent.controller.start();
		},false);
	};
	KnapsackView.prototype = Object.create(ModalView.prototype);
	KnapsackView.prototype.constructor = KnapsackView;
	// Controller
	function KnapsackController(parent){
		ModalController.apply(this, arguments);
	};
	KnapsackController.prototype = Object.create(ModalController.prototype);
	KnapsackController.prototype.constructor = KnapsackController;
	/************************************************/
	new Knapsack();
})();